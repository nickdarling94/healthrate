
INSERT INTO "HealthRate"."Musterija" ("telefon", "email", "ime", "prezime")
  VALUES ('111111', 'petar.peric@gmail.com', 'Petar', 'Peric');
INSERT INTO "HealthRate"."Musterija" ("telefon", "email", "ime", "prezime")
  VALUES ('222222', 'milan.markovic@gmail.com', 'Milan', 'Markovic');
INSERT INTO "HealthRate"."Musterija" ("telefon", "email", "ime", "prezime")
  VALUES ('333333', 'jovan.jocic@gmail.com', 'Jovan', 'Jocic');
INSERT INTO "HealthRate"."Musterija" ("telefon", "email", "ime", "prezime")
  VALUES ('444444', 'kosta.kostic@gmail.com', 'Kosta', 'Kostic');
INSERT INTO "HealthRate"."Musterija" ("telefon", "email", "ime", "prezime")
  VALUES ('555555', 'nikola.nikolic@gmail.com', 'Nikola', 'Nikolic');
INSERT INTO "HealthRate"."Musterija" ("telefon", "email", "ime", "prezime")
  VALUES ('666666', 'milan.markovic@gmail.com', 'Milan', 'Markovic');
INSERT INTO "HealthRate"."Musterija" ("telefon", "email", "ime", "prezime")
  VALUES ('777777', 'milan.markovic@gmail.com', 'Marko', 'Milanovic');
  
  
INSERT INTO "HealthRate"."Sauna" ("objekatID", "adresa", "grad", "naziv", "telefon", "brProstorija")
  VALUES ('000001', 'Knjazevacka 36', 'Topla noc', 'Nis', '222333', '3');
  
  
INSERT INTO "HealthRate"."Rezervacija" ("rezID", "vreme_dolaska", "vreme_odlaska", "radnikID", "naziv_usluge", "ocena_usluge", "objekatID", "telefon_musterija")
  VALUES ('000011', '16h', '18h', '011111', 'Masaza', '4', '101012', '111111');
INSERT INTO "HealthRate"."Rezervacija" ("rezID", "vreme_dolaska", "vreme_odlaska", "radnikID", "naziv_usluge", "ocena_usluge", "objekatID", "telefon_musterija")
  VALUES ('000000', '14h', '16h', '011113', 'Trening', '5', '101013', '222222');
INSERT INTO "HealthRate"."Rezervacija" ("rezID", "vreme_dolaska", "vreme_odlaska", "radnikID", "naziv_usluge", "ocena_usluge", "objekatID", "telefon_musterija")
  VALUES ('000001', '10h', '12h', '011112', 'Sauna', '2', '110011', '333333');
INSERT INTO "HealthRate"."Rezervacija" ("rezID", "vreme_dolaska", "vreme_odlaska", "radnikID", "naziv_usluge", "ocena_usluge", "objekatID", "telefon_musterija")
  VALUES ('000002', '11h', '12h', '011113', 'Plivanje', '1', '110012', '444444');
INSERT INTO "HealthRate"."Rezervacija" ("rezID", "vreme_dolaska", "vreme_odlaska", "radnikID", "naziv_usluge", "ocena_usluge", "objekatID", "telefon_musterija")
  VALUES ('000003', '11h', '12h', '011114', 'Trening', '5', '110113', '555555');

  
 
INSERT INTO "HealthRate"."Zaposleni" ("radnikID", "objekatID", "ime", "prezime", "pol", "godine_iskustva", "prosecna_ocena", "struka")
  VALUES ('011111', '101012', 'Jovan', 'Peric', 'muski', '6', '4.3', 'maser');
INSERT INTO "HealthRate"."Zaposleni" ("radnikID", "objekatID", "ime", "prezime", "pol", "godine_iskustva", "prosecna_ocena", "struka")
  VALUES ('011115', '101013', 'Milan', 'Jovic', 'muski', '7', '4.8', 'trener');
INSERT INTO "HealthRate"."Zaposleni" ("radnikID", "objekatID", "ime", "prezime", "pol", "godine_iskustva", "prosecna_ocena", "struka")
  VALUES ('011112', '101014', 'Zoran', 'Arsic', 'muski', '8', '5.0', 'instruktor plivanja');
INSERT INTO "HealthRate"."Zaposleni" ("radnikID", "objekatID", "ime", "prezime", "pol", "godine_iskustva", "prosecna_ocena", "struka")
  VALUES ('011113', '101012', 'Dusan', 'Milic', 'muski', '3', '3.3', 'radnik');
INSERT INTO "HealthRate"."Zaposleni" ("radnikID", "objekatID", "ime", "prezime", "pol", "godine_iskustva", "prosecna_ocena", "struka")
  VALUES ('011114', '101012', 'Milena', 'Kazic', 'zenski', '9', '2.3', 'maser');
  

  
INSERT INTO "HealthRate"."Teretana" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('101010', 'Gold', 'Prokuplje', 'Studenicka 14', '333444', 'crossfit');
INSERT INTO "HealthRate"."Teretana" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('101012', 'Elid', 'Kragujevac', 'Knjazevacka 22', '333444', 'crossfit');
INSERT INTO "HealthRate"."Teretana" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('101013', 'Fit', 'Kraljevo', 'Toplicina 8', '333444', 'crossfit');
INSERT INTO "HealthRate"."Teretana" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('101014', 'Olimp', 'Nis', 'Toplicka 2', '333444,' 'crossfit');
  
  
  
INSERT INTO "HealthRate"."Bazen" ("objekatID", "naziv", "grad", "adresa", "telefon", "brBazena", "tip")
  VALUES ('110110', 'Green Paradise', 'Nis', 'Topolska 18', '444555', '3', 'zatvoren');
INSERT INTO "HealthRate"."Bazen" ("objekatID", "naziv", "grad", "adresa", "telefon", "brBazena", "tip")
  VALUES ('110111', 'Kutina', 'Kutina', 'Topolska 18', '444556', '6', 'otvoren');
INSERT INTO "HealthRate"."Bazen" ("objekatID", "naziv", "grad", "adresa", "telefon", "brBazena", "tip")
  VALUES ('110112', 'Naiss', 'Nis', 'Topolska 18', '444557', '2', 'otvoren');
INSERT INTO "HealthRate"."Bazen" ("objekatID", "naziv", "grad", "adresa", "telefon", "brBazena", "tip")
  VALUES ('110113', 'Cair', 'Nis', 'Topolska 18', '444558', '3', 'zatvoren');
  
  
  
INSERT INTO "HealthRate"."Spa" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('110011', 'Dream Spa', 'Nis', 'Jugoviceva 33', '555666', 'Tajlandska masaza');
INSERT INTO "HealthRate"."Spa" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('110012', 'Health Spa', 'Krusevac', 'Jovana Ristica 33', '555667', 'Masaza toplim kamenjem');
INSERT INTO "HealthRate"."Spa" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('110013', 'Heaven Spa', 'Pirot', 'Studenicka 35', '555668', 'Sijacu masaza');
INSERT INTO "HealthRate"."Spa" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('110014', 'Sleep Spa', 'Zajecar', 'Jugoviceva 33', '555669', 'Kineska masaza');
INSERT INTO "HealthRate"."Spa" ("objekatID", "naziv", "grad", "adresa", "telefon", "tip")
  VALUES ('110015', 'Angle Spa', 'Beograd', 'Obilicev Venac 58', '555670', 'Nepalska masaza');