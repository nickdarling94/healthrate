CREATE TABLE "Musterija" (
    telefon text,
    email text,
    ime text,
    prezime text,
    PRIMARY KEY (telefon)
)
WITH
  bloom_filter_fp_chance=0.01
  AND compaction={ 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
  AND compression={ 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
  AND caching='{"keys":"ALL", "rows_per_partition":"NONE"}';


CREATE TABLE "Sauna" (
    "objekatID" text,
    adresa text,
    grad text,
    naziv text,
    telefon text,
    brProstorija text,
    PRIMARY KEY ("objekatID")
)
WITH
  bloom_filter_fp_chance=0.01
  AND compaction={ 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
  AND compression={ 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
  AND caching='{"keys":"ALL", "rows_per_partition":"NONE"}';


CREATE TABLE "Rezervacija" (
    "rezID" text,
    vreme_dolaska text,
    vreme_odlaska text,
    "radnikID" text,
    naziv_usluge text,
    ocena_usluge text,
    "objekatID" text,
    "telefon_musterija" text,
    PRIMARY KEY ("rezID")
)
WITH
  bloom_filter_fp_chance=0.01
  AND compaction={ 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
  AND compression={ 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
  AND caching='{"keys":"ALL", "rows_per_partition":"NONE"}';

CREATE TABLE "Zaposleni" (
    "radnikID" text,
    "objekatID" text,
    ime text,
    prezime text,
    pol text,
    godine_iskustva text,
    prosecna_ocena text,
    "struka" text,
    PRIMARY KEY ("radnikID", "objekatID")
)
WITH
  bloom_filter_fp_chance=0.01
  AND compaction={ 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
  AND compression={ 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
  AND caching='{"keys":"ALL", "rows_per_partition":"NONE"}';

  CREATE TABLE "Teretana" (
    "objekatID" text,
    naziv text,
	grad text,
	adresa text,
    telefon text,
    "tip" text,
    PRIMARY KEY ("objekatID")
)
WITH
  bloom_filter_fp_chance=0.01
  AND compaction={ 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
  AND compression={ 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
  AND caching='{"keys":"ALL", "rows_per_partition":"NONE"}';


  CREATE TABLE "Bazen" (
    "objekatID" text,
    naziv text,
	grad text,
	adresa text,
    telefon text,
	brBazena text,
    "tip" text,
    PRIMARY KEY ("objekatID")
)
WITH
  bloom_filter_fp_chance=0.01
  AND compaction={ 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
  AND compression={ 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
  AND caching='{"keys":"ALL", "rows_per_partition":"NONE"}';

  
CREATE TABLE "Spa" (
    "objekatID" text,
    naziv text,
	grad text,
	adresa text,
    telefon text,
    "tip" text,
    PRIMARY KEY ("objekatID")
)
WITH
  bloom_filter_fp_chance=0.01
  AND compaction={ 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
  AND compression={ 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
  AND caching='{"keys":"ALL", "rows_per_partition":"NONE"}';

