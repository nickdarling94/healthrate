﻿using HealthRate_Cassandra.QueryEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthRate_Cassandra
{
    public partial class UpravljanjeRezervacijama : Form
    {
        protected string[] rezervacijeIDs;
        protected string customerID;
        public UpravljanjeRezervacijama()
        {
            InitializeComponent();
        }
        public UpravljanjeRezervacijama(string id)
        {
            InitializeComponent();
            button3.Visible = false;
            List<Rezervacija> rezervacije = DataProvider.GetRezervacije();
            Musterija m = DataProvider.GetMusterija(id);
            int row = 0;
            foreach(Rezervacija r in rezervacije)
            {
                if (r.musterija == id)
                {
                    Zaposleni z = DataProvider.GetZaposlen(r.radnikID, r.objekatID);
                    dataGridView1.Rows[row].Cells[0].Value = m.ime + m.prezime;
                    dataGridView1.Rows[row].Cells[1].Value = z.ime + z.prezime;
                    dataGridView1.Rows[row].Cells[2].Value = r.vreme_dolaska;
                    dataGridView1.Rows[row].Cells[3].Value = r.vreme_odlaska;
                    dataGridView1.Rows[row].Cells[4].Value = r.naziv_usluge;
                    this.rezervacijeIDs[row] = r.rezID;
                    row++;
                }
                else MessageBox.Show("Ova musterija nema nijednu rezervaciju!");
            }

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 f = new Form1();
            f.Show();
        }

        private void UpravljanjeRezervacijama_Load(object sender, EventArgs e)
        {
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<Rezervacija> rezervacije = DataProvider.GetRezervacije();
            int row = 0;

            foreach (Rezervacija r in rezervacije)
            {
                Musterija m = DataProvider.GetMusterija(r.musterija);
                Zaposleni z = DataProvider.GetZaposlen(r.radnikID, r.objekatID);
                string ime = m.ime + m.prezime;
                dataGridView1.Rows[row].Cells[0].Value = ime;
                dataGridView1.Rows[row].Cells[1].Value = z.ime+z.prezime;
                dataGridView1.Rows[row].Cells[2].Value = r.vreme_dolaska;
                dataGridView1.Rows[row].Cells[3].Value = r.vreme_odlaska;
                dataGridView1.Rows[row].Cells[4].Value = r.naziv_usluge;
                this.rezervacijeIDs[row] = r.rezID;
                row++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count != 1) { MessageBox.Show("Selektujte rezervaciju za brisanje!"); }
            else
            {
                int index = dataGridView1.SelectedRows[0].Index;
                Rezervacija rez = DataProvider.GetRezervacija(rezervacijeIDs[index]);
                DataProvider.DeleteRezervacija(rez);
            }
        }
    }
}
