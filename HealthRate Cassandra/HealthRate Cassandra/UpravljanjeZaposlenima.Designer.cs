﻿namespace HealthRate_Cassandra
{
    partial class UpravljanjeZaposlenima
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.godine_iskustva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prosecna_ocena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.struka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ime,
            this.prezime,
            this.pol,
            this.godine_iskustva,
            this.prosecna_ocena,
            this.struka});
            this.dataGridView1.Location = new System.Drawing.Point(26, 44);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 288);
            this.dataGridView1.TabIndex = 0;
            // 
            // ime
            // 
            this.ime.HeaderText = "ime";
            this.ime.Name = "ime";
            // 
            // prezime
            // 
            this.prezime.HeaderText = "prezime";
            this.prezime.Name = "prezime";
            // 
            // pol
            // 
            this.pol.HeaderText = "pol";
            this.pol.Name = "pol";
            // 
            // godine_iskustva
            // 
            this.godine_iskustva.HeaderText = "godine_iskustva";
            this.godine_iskustva.Name = "godine_iskustva";
            // 
            // prosecna_ocena
            // 
            this.prosecna_ocena.HeaderText = "prosecna_ocena";
            this.prosecna_ocena.Name = "prosecna_ocena";
            // 
            // struka
            // 
            this.struka.HeaderText = "struka";
            this.struka.Name = "struka";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(787, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Nazad";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(736, 139);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 34);
            this.button2.TabIndex = 2;
            this.button2.Text = "Prikazi radnike";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(736, 192);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(87, 34);
            this.button4.TabIndex = 4;
            this.button4.Text = "Izmeni radnika";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(736, 242);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(87, 34);
            this.button5.TabIndex = 5;
            this.button5.Text = "Obrisi radnika";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // UpravljanjeZaposlenima
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(874, 363);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "UpravljanjeZaposlenima";
            this.Text = "UpravljanjeZaposlenima";
            this.Load += new System.EventHandler(this.UpravljanjeZaposlenima_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ime;
        private System.Windows.Forms.DataGridViewTextBoxColumn prezime;
        private System.Windows.Forms.DataGridViewTextBoxColumn pol;
        private System.Windows.Forms.DataGridViewTextBoxColumn godine_iskustva;
        private System.Windows.Forms.DataGridViewTextBoxColumn prosecna_ocena;
        private System.Windows.Forms.DataGridViewTextBoxColumn struka;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}