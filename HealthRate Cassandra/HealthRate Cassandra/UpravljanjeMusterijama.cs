﻿using HealthRate_Cassandra.QueryEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthRate_Cassandra
{
    public partial class UpravljanjeMusterijama : Form
    {
        public UpravljanjeMusterijama()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Musterija> musterije = DataProvider.GetMusterije();
            int row = 0;
            foreach(Musterija m in musterije)
            {
                dataGridView1.Rows[row].Cells[0].Value = m.ime;
                dataGridView1.Rows[row].Cells[1].Value = m.prezime;
                dataGridView1.Rows[row].Cells[2].Value = m.email;
                dataGridView1.Rows[row].Cells[3].Value = m.telefon;
                row++;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MusterijaForma f = new MusterijaForma("");
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int selected = dataGridView1.SelectedRows[0].Index;
            string id = dataGridView1.Rows[selected].Cells["Telefon"].ToString();
            MusterijaForma f = new MusterijaForma(id);
            f.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int selected = dataGridView1.SelectedRows[0].Index;
            string id = dataGridView1.Rows[selected].Cells["Telefon"].ToString();
            UpravljanjeRezervacijama rz = new UpravljanjeRezervacijama(id);
            rz.Show();
        }
    }
}
