﻿using HealthRate_Cassandra.QueryEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthRate_Cassandra
{
    public partial class RadnikForma : Form
    {
        protected string radnikID;
        protected string objekatID;

        public RadnikForma(string r, string o)
        {
            radnikID = r;
            objekatID = o;
            InitializeComponent();
        }

        private void RadnikForma_Load(object sender, EventArgs e)
        {
            if (radnikID != "" && objekatID != "")
            {
                Zaposleni z = DataProvider.GetZaposlen(radnikID, objekatID);
                textBox1.Text = z.ime;
                textBox2.Text = z.prezime;
                if (z.pol == "muski")
                    radioButton1.Checked = true;
                else radioButton2.Checked = true;
                textBox3.Text = z.godine_iskustva;
                switch (z.struka)
                {
                    case "trener": { comboBox1.SelectedValue = "trener"; break; }
                    case "instruktor plivanja": { comboBox1.SelectedValue = "instruktor plivanja"; break; }
                    case "maser": { comboBox1.SelectedValue = "maser"; break; }
                    default: { comboBox1.SelectedValue = "radnik"; break; }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Zaposleni z = new QueryEntities.Zaposleni();
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                z.ime = textBox1.Text;
                z.prezime = textBox2.Text;
                if (radioButton1.Checked)
                    z.pol = "muski";
                else if (radioButton2.Checked)
                    z.pol = "zenski";
                else MessageBox.Show("Odaberite pol!");
                z.godine_iskustva = textBox3.Text;
                z.prosecna_ocena = 0.0;
                z.objekatID = objekatID;

                DataProvider.AddZaposlen(z);
            }
            else MessageBox.Show("Unesite sve trazene podatke!");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Zaposleni z = DataProvider.GetZaposlen(radnikID, objekatID);
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                z.ime = textBox1.Text;
                z.prezime = textBox2.Text;
                if (radioButton1.Checked)
                    z.pol = "muski";
                else if (radioButton2.Checked)
                    z.pol = "zenski";
                else MessageBox.Show("Odaberite pol!");
                z.godine_iskustva = textBox3.Text;
                z.prosecna_ocena = 0.0;
                z.objekatID = objekatID;

                DataProvider.PutZaposlen(z);
            }
            else MessageBox.Show("Unesite sve trazene podatke!");
        }
    }
}
