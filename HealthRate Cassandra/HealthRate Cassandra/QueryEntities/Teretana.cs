﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthRate_Cassandra.QueryEntities
{
    class Teretana
    {
        public string objekatID { get; set; }
        public string naziv { get; set; }
        public string grad { get; set; }
        public string adresa { get; set; }
        public string telefon { get; set; }
        public string tip { get; set; }
    }
}
