﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthRate_Cassandra.QueryEntities
{
    class Musterija
    {
        public string telefon { get; set; }
        public string email { get; set; }
        public string ime { get; set; }
        public string prezime { get; set; }

        public List<Rezervacija> rezervacije { get; set; }
    }
}
