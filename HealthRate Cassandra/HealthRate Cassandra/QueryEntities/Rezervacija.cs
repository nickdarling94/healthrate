﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthRate_Cassandra.QueryEntities
{
    class Rezervacija
    {
        public string rezID { get; set; }
        public string vreme_dolaska { get; set; }
        public string vreme_odlaska { get; set; }
        public string naziv_usluge { get; set; }
        public string radnikID { get; set; }
        public string objekatID { get; set; }
        public string ocena_usluge { get; set; }
        public string musterija { get; set; }
    }
}
