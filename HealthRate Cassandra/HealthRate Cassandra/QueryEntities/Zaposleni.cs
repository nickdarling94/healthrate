﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthRate_Cassandra.QueryEntities
{
    class Zaposleni
    {
        public string radnikID { get; set; }
        public string objekatID { get; set; }
        public string ime { get; set; }
        public string prezime { get; set; }
        public string pol { get; set; }
        public string godine_iskustva { get; set; }
        public double prosecna_ocena { get; set; }
        public string struka { get; set; }
    }
}
