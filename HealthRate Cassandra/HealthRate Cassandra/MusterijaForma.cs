﻿using HealthRate_Cassandra.QueryEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthRate_Cassandra
{
    public partial class MusterijaForma : Form
    {
        protected string customerID;
        public MusterijaForma(string id)
        {
            InitializeComponent();
            customerID = id;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e) //izmeni
        {
            Musterija m = DataProvider.GetMusterija(customerID);
            m.ime = textBox1.Text;
            m.prezime = textBox2.Text;
            m.telefon = textBox3.Text;
            m.email = textBox4.Text;
            DataProvider.PutMusterija(m);
        }

        private void button2_Click(object sender, EventArgs e) //dodaj
        {
            Musterija m = new QueryEntities.Musterija();
            m.ime = textBox1.Text;
            m.prezime = textBox2.Text;
            m.telefon = textBox3.Text;
            m.email = textBox4.Text;
            DataProvider.AddMusterija(m);
        }

        private void MusterijaForma_Load(object sender, EventArgs e)
        {
            if(customerID != "")
            {
                Musterija m = DataProvider.GetMusterija(customerID);
                textBox1.Text = m.ime;
                textBox2.Text = m.prezime;
                textBox3.Text = m.telefon;
                textBox4.Text = m.email;
            }
        }
    }
}
