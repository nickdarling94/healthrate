﻿using HealthRate_Cassandra.QueryEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthRate_Cassandra
{
    public partial class UpravljanjeZaposlenima : Form
    {
        protected Hashtable[] workers;
        //koristi se da upamti parove radnikID i objekatID zbog prosledjivanja medju formama
        public UpravljanjeZaposlenima()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 forma = new HealthRate_Cassandra.Form1();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Zaposleni> radnici = DataProvider.GetZaposleni();
            
            int row = 0;
            foreach (Zaposleni r in radnici)
            {
                dataGridView1.Rows[row].Cells[0].Value = r.ime;
                dataGridView1.Rows[row].Cells[1].Value = r.prezime;
                dataGridView1.Rows[row].Cells[2].Value = r.pol;
                dataGridView1.Rows[row].Cells[3].Value = r.godine_iskustva;
                dataGridView1.Rows[row].Cells[4].Value = r.prosecna_ocena;
                workers[row].Add(r.radnikID, r.objekatID);
                row++;
            }
        }


        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1) { MessageBox.Show("Selektujte jednog radnika za izmenu!"); }
            else
            {
                int id = dataGridView1.CurrentCell.RowIndex;
                string r = workers[id].Keys.ToString();
                string o = workers[id].Values.ToString();
                RadnikForma rF = new RadnikForma(r, o);
                rF.Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1) { MessageBox.Show("Selektujte jednog radnika za brisanje!"); }
            else
            {
                int id = dataGridView1.CurrentCell.RowIndex;
                string r = workers[id].Keys.ToString();
                string o = workers[id].Values.ToString();
                DataProvider.DeleteZaposlen(r, o);
                workers[id].Remove(r);
            }
        }

        private void UpravljanjeZaposlenima_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }

}
