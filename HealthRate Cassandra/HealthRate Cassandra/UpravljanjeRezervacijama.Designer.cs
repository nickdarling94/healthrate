﻿namespace HealthRate_Cassandra
{
    partial class UpravljanjeRezervacijama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpravljanjeRezervacijama));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Musterija = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Radnik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vreme_dolaska = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vreme_odlaska = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Naziv_usluge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Musterija,
            this.Radnik,
            this.Vreme_dolaska,
            this.Vreme_odlaska,
            this.Naziv_usluge});
            this.dataGridView1.Location = new System.Drawing.Point(35, 43);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(545, 274);
            this.dataGridView1.TabIndex = 0;
            // 
            // Musterija
            // 
            this.Musterija.HeaderText = "Musterija";
            this.Musterija.Name = "Musterija";
            // 
            // Radnik
            // 
            this.Radnik.HeaderText = "Radnik";
            this.Radnik.Name = "Radnik";
            // 
            // Vreme_dolaska
            // 
            this.Vreme_dolaska.HeaderText = "Vreme_dolaska";
            this.Vreme_dolaska.Name = "Vreme_dolaska";
            // 
            // Vreme_odlaska
            // 
            this.Vreme_odlaska.HeaderText = "Vreme_odlaska";
            this.Vreme_odlaska.Name = "Vreme_odlaska";
            // 
            // Naziv_usluge
            // 
            this.Naziv_usluge.HeaderText = "Naziv_usluge";
            this.Naziv_usluge.Name = "Naziv_usluge";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(634, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 45);
            this.button1.TabIndex = 1;
            this.button1.Text = "Obrisi rezervaciju";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(660, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Nazad";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(634, 126);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 45);
            this.button3.TabIndex = 3;
            this.button3.Text = "Prikazi rezervacije";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // UpravljanjeRezervacijama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(743, 346);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "UpravljanjeRezervacijama";
            this.Text = "UpravljanjeRezervacijama";
            this.Load += new System.EventHandler(this.UpravljanjeRezervacijama_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Musterija;
        private System.Windows.Forms.DataGridViewTextBoxColumn Radnik;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vreme_dolaska;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vreme_odlaska;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv_usluge;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}