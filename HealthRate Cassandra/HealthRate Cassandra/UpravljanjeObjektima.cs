﻿using HealthRate_Cassandra.QueryEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthRate_Cassandra
{
    public partial class UpravljanjeObjektima : Form
    {
        protected string kljuc; //koristi se kao kljuc za novi objekat pri ubacivanju u bazu
        //pamtimo zbog kasnijeg pristupa ID-ju objekata
        protected string[] saunaIDs;
        protected string[] gymIDs;
        protected string[] poolIDs;
        protected string[] spaIDs;
        public UpravljanjeObjektima()
        {
            InitializeComponent();
            kljuc = generateKey();
        }

        private string generateKey()
        {
            kljuc = "1234";
            int k = Int32.Parse(kljuc);
            k++;
            kljuc = k.ToString();
            return kljuc;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1) MessageBox.Show("Selektujte jednu saunu!");
            else
            {
                string saunaID = saunaIDs[dataGridView1.SelectedRows[0].Index];
            
                RadnikForma rF = new HealthRate_Cassandra.RadnikForma(kljuc, saunaID);
                int k = Int32.Parse(kljuc);
                k++;
                kljuc = k.ToString();
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            Form1 forma = new Form1();
            forma.Show();
            this.Close();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            List<Sauna> saune = DataProvider.GetSaune();
            int r = 0;
            foreach (Sauna s in saune)
            {

                dataGridView1.Rows[r].Cells[0].Value = s.naziv;
                dataGridView1.Rows[r].Cells[1].Value = s.adresa;
                dataGridView1.Rows[r].Cells[2].Value = s.grad;
                dataGridView1.Rows[r].Cells[3].Value = s.telefon;
                dataGridView1.Rows[r].Cells[4].Value = s.brProstorija;
                saunaIDs[r] = s.objekatID;
                r++;
            }

            List<Teretana> gyms = DataProvider.GetGyms();
            int r1 = 0;
            foreach (Teretana t in gyms)
            {

                dataGridView2.Rows[r1].Cells[0].Value = t.naziv;
                dataGridView2.Rows[r1].Cells[1].Value = t.adresa;
                dataGridView2.Rows[r1].Cells[2].Value = t.grad;
                dataGridView2.Rows[r1].Cells[3].Value = t.telefon;
                dataGridView2.Rows[r1].Cells[4].Value = t.tip;
                gymIDs[r1] = t.objekatID;
                r1++;
            }

            List<Bazen> pools = DataProvider.GetBazeni();
            int r2 = 0;
            foreach (Bazen p in pools)
            {

                dataGridView3.Rows[r2].Cells[0].Value = p.naziv;
                dataGridView3.Rows[r2].Cells[1].Value = p.adresa;
                dataGridView3.Rows[r2].Cells[2].Value = p.grad;
                dataGridView3.Rows[r2].Cells[3].Value = p.telefon;
                dataGridView3.Rows[r2].Cells[4].Value = p.tip;
                dataGridView3.Rows[r2].Cells[5].Value = p.brBazena;
                poolIDs[r2] = p.objekatID;
                r2++;
            }

            List<Spa> spaCentri = DataProvider.GetSpaCentri();
            int r3 = 0;
            foreach (Spa sp in spaCentri)
            {

                dataGridView4.Rows[r3].Cells[0].Value = sp.naziv;
                dataGridView4.Rows[r3].Cells[1].Value = sp.adresa;
                dataGridView4.Rows[r3].Cells[2].Value = sp.grad;
                dataGridView4.Rows[r3].Cells[3].Value = sp.telefon;
                dataGridView4.Rows[r3].Cells[4].Value = sp.tip;
                spaIDs[r3] = sp.objekatID;
                r3++;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DodajObjekat d = new DodajObjekat("a1", 0);
            d.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1) MessageBox.Show("Selektujte jednu saunu za izmenu!");
            else
            {
                string saunaID = saunaIDs[dataGridView1.SelectedRows[0].Index];

                DodajObjekat d = new DodajObjekat(saunaID, 1);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1) MessageBox.Show("Selektujte jednu saunu za brisanje!");
            else
            {
                string saunaID = saunaIDs[dataGridView1.SelectedRows[0].Index];
                Sauna s = DataProvider.GetSauna(saunaID);
                DataProvider.DeleteSauna(s);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count != 1) MessageBox.Show("Selektujte jednu teretanu!");
            else
            {
                string gymID = gymIDs[dataGridView2.SelectedRows[0].Index];

                RadnikForma rF = new HealthRate_Cassandra.RadnikForma(kljuc, gymID);
                int k = Int32.Parse(kljuc);
                k++;
                kljuc = k.ToString();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DodajObjekat d = new DodajObjekat("b2", 0);
            d.Show();
            this.Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count != 1) MessageBox.Show("Selektujte jednu teretanu za izmenu!");
            else
            {
                string gymID = gymIDs[dataGridView2.SelectedRows[0].Index];

                DodajObjekat d = new DodajObjekat(gymID, 2);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count != 1) MessageBox.Show("Selektujte jednu teretanu za brisanje!");
            else
            {
                string gymID = gymIDs[dataGridView2.SelectedRows[0].Index];
                Teretana g = DataProvider.GetGym(gymID);
                DataProvider.DeleteGym(g);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (dataGridView3.SelectedRows.Count != 1) MessageBox.Show("Selektujte jedan bazen!");
            else
            {
                string poolID = poolIDs[dataGridView3.SelectedRows[0].Index];

                RadnikForma rF = new HealthRate_Cassandra.RadnikForma(kljuc, poolID);
                int k = Int32.Parse(kljuc);
                k++;
                kljuc = k.ToString();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            DodajObjekat d = new DodajObjekat("c3", 0);
            d.Show();
            this.Hide();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (dataGridView3.SelectedRows.Count != 1) MessageBox.Show("Selektujte jedan bazen za izmenu!");
            else
            {
                string pID = poolIDs[dataGridView3.SelectedRows[0].Index];

                DodajObjekat d = new DodajObjekat(pID, 3);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (dataGridView3.SelectedRows.Count != 1) MessageBox.Show("Selektujte jedan bazen za brisanje!");
            else
            {
                string pID = poolIDs[dataGridView3.SelectedRows[0].Index];
                Bazen b = DataProvider.GetBazen(pID);
                DataProvider.DeleteBazen(b);
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (dataGridView4.SelectedRows.Count != 1) MessageBox.Show("Selektujte jedan Spa Centar!");
            else
            {
                string sID = spaIDs[dataGridView4.SelectedRows[0].Index];

                RadnikForma rF = new HealthRate_Cassandra.RadnikForma(kljuc, sID);
                int k = Int32.Parse(kljuc);
                k++;
                kljuc = k.ToString();
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            DodajObjekat d = new DodajObjekat("d4", 0);
            d.Show();
            this.Hide();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (dataGridView4.SelectedRows.Count != 1) MessageBox.Show("Selektujte jedan Spa Centar za izmenu!");
            else
            {
                string sID = spaIDs[dataGridView4.SelectedRows[0].Index];

                DodajObjekat d = new DodajObjekat(sID, 4);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (dataGridView4.SelectedRows.Count != 1) MessageBox.Show("Selektujte jedan Spa Centar za brisanje!");
            else
            {
                string sID = spaIDs[dataGridView4.SelectedRows[0].Index];
                Spa s = DataProvider.GetSpaCentar(sID);
                DataProvider.DeleteSpa(s);
            }
        }
    }
}
