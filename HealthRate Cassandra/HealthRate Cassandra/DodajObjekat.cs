﻿using HealthRate_Cassandra.QueryEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthRate_Cassandra
{
    public partial class DodajObjekat : Form
    {
        protected string tip;
        protected string objekatID;
        public DodajObjekat(string s, int obj)
        {
            InitializeComponent();
            if (obj == 0)
            {
                switch (s)
                {
                    case "a1": { tip = "sauna"; break; }
                    case "b2": { tip = "teretana"; break; }
                    case "c3": { tip = "bazen"; break; }
                    case "d4": { tip = "spa"; break; }
                }
            }
            else if (obj == 1)
                tip = "saunaUpdate";
            else if (obj == 2)
                tip = "teretanaUpdate"; 
            else if (obj == 3)
                tip = "bazenUpdate";
            else if (obj == 4)
                tip = "spaUpdate";
            objekatID = s;
        }

        private void DodajObjekat_Load(object sender, EventArgs e)
        {
            if (tip == "saunaUpdate")
            {
                Sauna s = DataProvider.GetSauna(objekatID);
                textBox1.Text = s.naziv;
                textBox2.Text = s.adresa;
                textBox3.Text = s.grad;
                textBox4.Text = s.telefon;
                panel2.Enabled = false;
                panel3.Enabled = false;
                panel4.Enabled = false;
                textBox5.Text = s.brProstorija.ToString();
            }
            if (tip == "teretanaUpdate")
            {
                Teretana s = DataProvider.GetGym(objekatID);
                textBox1.Text = s.naziv;
                textBox2.Text = s.adresa;
                textBox3.Text = s.grad;
                textBox4.Text = s.telefon;
                panel1.Enabled = false;
                panel3.Enabled = false;
                panel4.Enabled = false;
                if (s.tip == "crossfit")
                    comboBox1.SelectedValue = "crossfit";
                else if (s.tip == "bodyweight")
                    comboBox1.SelectedValue = "bodyweight";
                else comboBox1.SelectedValue = "sprave";
            }
            if (tip == "bazenUpdate")
            {
                Bazen s = DataProvider.GetBazen(objekatID);
                textBox1.Text = s.naziv;
                textBox2.Text = s.adresa;
                textBox3.Text = s.grad;
                textBox4.Text = s.telefon;
                panel1.Enabled = false;
                panel2.Enabled = false;
                panel4.Enabled = false;
                if (s.tip == "otvoren")
                    comboBox2.SelectedValue = "otvoren";
                else comboBox2.SelectedValue = "zatvoren";
                textBox6.Text = s.brBazena.ToString();
            }
            if (tip == "spaUpdate")
            {
                Spa s = DataProvider.GetSpaCentar(objekatID);
                textBox1.Text = s.naziv;
                textBox2.Text = s.adresa;
                textBox3.Text = s.grad;
                textBox4.Text = s.telefon;
                panel1.Enabled = false;
                panel2.Enabled = false;
                panel3.Enabled = false;
                switch(s.tip)
                {
                    case "Tajlandska masaza": { comboBox3.SelectedIndex = 0; break; }
                    case "Kineska masaza": { comboBox3.SelectedIndex = 1; break; }
                    case "Nepalska masaza": { comboBox3.SelectedIndex = 2; break; }
                    case "Sijacu masaza": { comboBox3.SelectedIndex = 3; break; }
                    case "Masaza toplim kamenjem": { comboBox3.SelectedIndex = 4; break; }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tip == "sauna")
            {
                Sauna s = new QueryEntities.Sauna();
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                s.brProstorija = Int32.Parse(textBox5.Text);
                DataProvider.AddSauna(s);
            }
            if (tip == "teretana")
            {
                Teretana s = new QueryEntities.Teretana();
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                if (comboBox1.SelectedValue.ToString() == "crossfit")
                    s.tip = "crossfit";
                else if (comboBox1.SelectedValue.ToString() == "bodyweight")
                    s.tip = "bodyweight";
                else s.tip = "sprave";
                DataProvider.AddGym(s);
            }
            if (tip == "bazen")
            {
                Bazen s = new Bazen();
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                if (comboBox2.SelectedValue.ToString() == "otvoren")
                    s.tip = "otvoren";
                else s.tip = "zatvoren";
                s.brBazena = Int32.Parse(textBox6.Text);
                DataProvider.AddBazen(s);
            }
            if (tip == "spa")
            {
                Spa s = new QueryEntities.Spa();
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                switch (comboBox3.SelectedIndex)
                {
                    case 0: { s.tip = "Tajlandska masaza"; break; }
                    case 1: { s.tip = "Kineska masaza"; break; }
                    case 2: { s.tip = "Nepalska masaza"; break; }
                    case 3: { s.tip = "Sijacu masaza"; break; }
                    case 4: { s.tip = "Masaza toplim kamenjem"; break; }
                }
                DataProvider.AddSpa(s);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (tip == "saunaUpdate")
            {
                Sauna s = DataProvider.GetSauna(objekatID);
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                s.brProstorija = Int32.Parse(textBox5.Text);
                DataProvider.PutSauna(s);
            }
            if (tip == "teretanaUpdate")
            {
                Teretana s = DataProvider.GetGym(objekatID);
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                if (comboBox1.SelectedValue.ToString() == "crossfit")
                    s.tip = "crossfit";
                else if (comboBox1.SelectedValue.ToString() == "bodyweight")
                    s.tip = "bodyweight";
                else s.tip = "sprave";
                DataProvider.PutGym(s);
            }
            if (tip == "bazenUpdate")
            {
                Bazen s = DataProvider.GetBazen(objekatID);
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                if (comboBox2.SelectedValue.ToString() == "otvoren")
                    s.tip = "otvoren";
                else s.tip = "zatvoren";
                s.brBazena = Int32.Parse(textBox6.Text);
                DataProvider.PutBazen(s);
            }
            if (tip == "spaUpdate")
            {
                Spa s = DataProvider.GetSpaCentar(objekatID);
                s.naziv = textBox1.Text;
                s.adresa = textBox2.Text;
                s.grad = textBox3.Text;
                s.telefon = textBox4.Text;
                switch (comboBox3.SelectedIndex)
                {
                    case 0: { s.tip = "Tajlandska masaza"; break; }
                    case 1: { s.tip = "Kineska masaza"; break; }
                    case 2: { s.tip = "Nepalska masaza"; break; }
                    case 3: { s.tip = "Sijacu masaza"; break; }
                    case 4: { s.tip = "Masaza toplim kamenjem"; break; }
                }
                DataProvider.PutSpa(s);
            }
            }
    }
}
