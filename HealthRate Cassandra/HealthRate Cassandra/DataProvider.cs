﻿using HealthRate_Cassandra.QueryEntities;
using System;
using Cassandra;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HealthRate_Cassandra
{
    class DataProvider
    {

        //2. za svaku vrednost kada se cita proveri se da li postoji, ako ne: klijent vraca null, a cassandra ne pamti nista
        #region Bazen
        public static Bazen GetBazen(string objekatID)
        {
            ISession session = SessionManager.GetSession();
            Bazen bazen = new Bazen();

            if (session == null)
                return null;

            Row bazenData = session.Execute("select * from \"HealthRate\".\"Bazen\" where \"objekatID\"=" + objekatID + "\\").FirstOrDefault();

            if (bazenData != null)
            {
                bazen.objekatID = bazenData["objekatID"] != null ? bazenData["objekatID"].ToString() : string.Empty;
                bazen.naziv = bazenData["naziv"] != null ? bazenData["naziv"].ToString() : string.Empty;
                bazen.grad = bazenData["grad"] != null ? bazenData["grad"].ToString() : string.Empty;
                bazen.adresa = bazenData["adresa"] != null ? bazenData["adresa"].ToString() : string.Empty;
                bazen.telefon = bazenData["telefon"] != null ? bazenData["telefon"].ToString() : string.Empty;
                string broj = bazenData["brBazena"] != null ? bazenData["BrBazena"].ToString() : string.Empty;
                if (broj != "")
                    bazen.brBazena = Int32.Parse(broj);
                else bazen.brBazena = 0;
                bazen.tip = bazenData["tip"] != null ? bazenData["tip"].ToString() : string.Empty;
            }

            return bazen;
        }

        public static List<Bazen> GetBazeni()
        {
            ISession session = SessionManager.GetSession();
            List<Bazen> bazeni = new List<Bazen>();

            if (session == null)
                return null;

            RowSet bazeniData = session.Execute("select * from \"HealthRate\".\"Bazen\"");

            if (bazeniData.Count() > 0)
            {
                foreach (Row bazenData in bazeniData)
                {
                    Bazen bazen = new Bazen();
                    bazen.objekatID = bazenData["objekatID"] != null ? bazenData["objekatID"].ToString() : string.Empty;
                    bazen.adresa = bazenData["adresa"] != null ? bazenData["adresa"].ToString() : string.Empty;
                    bazen.grad = bazenData["grad"] != null ? bazenData["grad"].ToString() : string.Empty;
                    bazen.naziv = bazenData["naziv"] != null ? bazenData["naziv"].ToString() : string.Empty;
                    bazen.telefon = bazenData["telefon"] != null ? bazenData["telefon"].ToString() : string.Empty;
                    bazen.tip = bazenData["tip"] != null ? bazenData["tip"].ToString() : string.Empty;
                    string broj = bazenData["brBazena"] != null ? bazenData["brBazena"].ToString() : string.Empty;
                    if (broj != "")
                        bazen.brBazena = Int32.Parse(broj);
                    else bazen.brBazena = 0;
                    bazeni.Add(bazen);
                }

            }

            return bazeni;
        }

        public static void AddBazen(Bazen b)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
            string query = "insert into \"HealthRate\".\"Bazen\" (\"objekatID\", adresa, grad, naziv, telefon, brBazena)  values ('" + b.objekatID + "','" + b.adresa +"','" + b.grad+ "','" + b.naziv+ "','" + b.telefon + b.brBazena.ToString()+ "')";
            RowSet bazenData = session.Execute(query);
        }

        public static void DeleteBazen(Bazen b)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet bazenData = session.Execute("delete from \"HealthRate\".\"Bazen\" where \"objekatID\" = '" + b.objekatID + "'");

        }

        public static void PutBazen(Bazen b)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1 = "UPDATE \"HealthRate\".\"Bazen\" SET \"adresa\" = '" + b.adresa + "'"+ "grad\" = '" + b.grad + "'"+ "naziv\" = '" + b.naziv + "'" +"brBazena\" = '" +b.brBazena.ToString() +"'" + "telefon\" = '"+b.telefon +"' WHERE \"objekatID\" = '" + b.objekatID + "'";
            RowSet bazenData = session.Execute(query1);

        }

        #endregion

        #region Spa
        public static Spa GetSpaCentar(string objekatID)
        {
            ISession session = SessionManager.GetSession();
            Spa spa_centar = new Spa();

            if (session == null)
                return null;

            Row spaCentarData = session.Execute("select * from \"HealthRate\".\"Spa\" where \"objekatID\"=" + objekatID + "\\").FirstOrDefault();

            if (spaCentarData != null)
            {
                spa_centar.objekatID = spaCentarData["objekatID"] != null ? spaCentarData["objekatID"].ToString() : string.Empty;
                spa_centar.adresa = spaCentarData["adresa"] != null ? spaCentarData["adresa"].ToString() : string.Empty;
                spa_centar.grad = spaCentarData["grad"] != null ? spaCentarData["grad"].ToString() : string.Empty;
                spa_centar.naziv = spaCentarData["naziv"] != null ? spaCentarData["naziv"].ToString() : string.Empty;
                spa_centar.telefon = spaCentarData["telefon"] != null ? spaCentarData["telefon"].ToString() : string.Empty;
                spa_centar.tip = spaCentarData["tip"] != null ? spaCentarData["tip"].ToString() : string.Empty;
            }

            return spa_centar;
        }

        public static List<Spa> GetSpaCentri()
        {
            ISession session = SessionManager.GetSession();
            List<Spa> spa_centri = new List<Spa>();


            if (session == null)
                return null;

            RowSet spaCentriData = session.Execute("select * from \"HealthRate\".\"Spa\"");

            if (spaCentriData.Count() > 0)
            {
                foreach (Row spaCentarData in spaCentriData)
                {
                    Spa spa_centar = new Spa();
                    spa_centar.objekatID = spaCentarData["objekatID"] != null ? spaCentarData["objekatID"].ToString() : string.Empty;
                    spa_centar.adresa = spaCentarData["adresa"] != null ? spaCentarData["adresa"].ToString() : string.Empty;
                    spa_centar.grad = spaCentarData["grad"] != null ? spaCentarData["grad"].ToString() : string.Empty;
                    spa_centar.naziv = spaCentarData["naziv"] != null ? spaCentarData["naziv"].ToString() : string.Empty;
                    spa_centar.telefon = spaCentarData["telefon"] != null ? spaCentarData["telefon"].ToString() : string.Empty;
                    spa_centar.tip = spaCentarData["tip"] != null ? spaCentarData["tip"].ToString() : string.Empty;
                    spa_centri.Add(spa_centar);
                }

            }

            return spa_centri;
        }

        public static void AddSpa(Spa spa)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1 = "insert into \"HealthRate\".\"Spa\" (\"objekatID\", adresa, grad, naziv, telefon, tip)  values ('" + spa.objekatID + "','" + spa.adresa + "','" + spa.grad + "','" + spa.naziv + "','" + spa.telefon + "','" + spa.tip +"')";
            RowSet spaCentarData = session.Execute(query1);

        }

        public static void DeleteSpa(Spa spa)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet spaCentarData = session.Execute("delete from \"HealthRate\".\"Spa\" where \"objekatID\" = '" + spa.objekatID + "'");

        }
        public static void PutSpa(Spa s)
        {
            ISession session = SessionManager.GetSession();
            
            if (session == null)
                return;

            string query1 = "UPDATE \"HealthRate\".\"Spa\" SET \"adresa\" = '" + s.adresa + "'" + "grad\" = '" + s.grad + "'" + "naziv\" = '" + s.naziv + "'" + "tip\" = '" + s.tip + "'" + "telefon\" = '" + s.telefon + "' WHERE \"objekatID\" = '" + s.objekatID + "'";
            RowSet bazenData = session.Execute(query1);

        }

        #endregion

        #region Sauna
        public static Sauna GetSauna(string objekatID)
        {
            ISession session = SessionManager.GetSession();
            Sauna sauna = new Sauna();

            if (session == null)
                return null;

            Row saunaData = session.Execute("select * from \"HealthRate\".\"Sauna\" where \"objekatID\"=" + objekatID + "\\").FirstOrDefault();

            if (saunaData != null)
            {
                sauna.objekatID = saunaData["objekatID"] != null ? saunaData["objekatID"].ToString() : string.Empty;
                sauna.adresa = saunaData["adresa"] != null ? saunaData["adresa"].ToString() : string.Empty;
                sauna.grad = saunaData["grad"] != null ? saunaData["grad"].ToString() : string.Empty;
                sauna.naziv = saunaData["naziv"] != null ? saunaData["naziv"].ToString() : string.Empty;
                sauna.telefon = saunaData["telefon"] != null ? saunaData["telefon"].ToString() : string.Empty;
                string broj = saunaData["brProstorija"] != null ? saunaData["brProstorija"].ToString() : string.Empty;
                if (broj != "")
                    sauna.brProstorija = Int32.Parse(broj);
                else sauna.brProstorija = 0;
            }

            return sauna;
        }

        public static List<Sauna> GetSaune()
        {
            ISession session = SessionManager.GetSession();
            List<Sauna> saune = new List<Sauna>();


            if (session == null)
                return null;

            RowSet sauneData = session.Execute("select * from \"HealthRate\".\"Sauna\"");

            if (sauneData.Count() > 0)
            {
                foreach (Row saunaData in sauneData)
                {
                    Sauna sauna = new Sauna();
                    sauna.objekatID = saunaData["objekatID"] != null ? saunaData["objekatID"].ToString() : string.Empty;
                    sauna.adresa = saunaData["adresa"] != null ? saunaData["adresa"].ToString() : string.Empty;
                    sauna.grad = saunaData["grad"] != null ? saunaData["grad"].ToString() : string.Empty;
                    sauna.naziv = saunaData["naziv"] != null ? saunaData["naziv"].ToString() : string.Empty;
                    sauna.telefon = saunaData["telefon"] != null ? saunaData["telefon"].ToString() : string.Empty;
                    string broj = saunaData["brProstorija"] != null ? saunaData["brProstorija"].ToString() : string.Empty;
                    if (broj != "")
                        sauna.brProstorija = Int32.Parse(broj);
                    else sauna.brProstorija = 0;
                    saune.Add(sauna);
                }

            }

            return saune;
        }

        public static void AddSauna(Sauna sauna)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
            string query = "insert into \"HealthRate\".\"Bazen\" (\"objekatID\", adresa, grad, naziv, telefon, brProstorija)  values ('" + sauna.objekatID + "','" + sauna.adresa + "','" + sauna.grad + "','" + sauna.naziv + "','" + sauna.telefon + "','" + sauna.brProstorija.ToString() + "')";
            RowSet saunaData = session.Execute(query); 

        }

        public static void DeleteSauna(Sauna sauna)
        {
            ISession session = SessionManager.GetSession();
            
            if (session == null)
                return;

            RowSet saunaData = session.Execute("delete from \"HealthRate\".\"Sauna\" where \"objekatID\" = '" + sauna.objekatID + "'");

        }

        public static void PutSauna(Sauna s)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1 = "UPDATE \"HealthRate\".\"Spa\" SET \"adresa\" = '" + s.adresa + "'" + "grad\" = '" + s.grad + "'" + "naziv\" = '" + s.naziv + "'" + "brProstorija\" = '" + s.brProstorija.ToString() + "'" + "telefon\" = '" + s.telefon + "' WHERE \"objekatID\" = '" + s.objekatID + "'";
            RowSet saunaData = session.Execute(query1);

        }
        #endregion


        #region Teretana
        public static Teretana GetGym(string objekatID)
        {
            ISession session = SessionManager.GetSession();
            Teretana gym = new Teretana();

            if (session == null)
                return null;

            Row gymData = session.Execute("select * from \"HealthRate\".\"Teretana\" where \"objekatID\"=" + objekatID + "\\").FirstOrDefault();

            if (gymData != null)
            {
                gym.objekatID = gymData["objekatID"] != null ? gymData["objekatID"].ToString() : string.Empty;
                gym.adresa = gymData["adresa"] != null ? gymData["adresa"].ToString() : string.Empty;
                gym.grad = gymData["grad"] != null ? gymData["grad"].ToString() : string.Empty;
                gym.naziv = gymData["naziv"] != null ? gymData["naziv"].ToString() : string.Empty;
                gym.telefon = gymData["telefon"] != null ? gymData["telefon"].ToString() : string.Empty;
                gym.tip = gymData["tip"] != null ? gymData["tip"].ToString() : string.Empty;
            }

            return gym;
        }

        public static List<Teretana> GetGyms()
        {
            ISession session = SessionManager.GetSession();
            List<Teretana> teretane = new List<Teretana>();


            if (session == null)
                return null;

            RowSet gymsData = session.Execute("select * from \"HealthRate\".\"Teretana\"");

            if (gymsData.Count() > 0)
            {
                foreach (Row gymData in gymsData)
                {
                    Teretana gym = new Teretana();
                    gym.objekatID = gymData["objekatID"] != null ? gymData["objekatID"].ToString() : string.Empty;
                    gym.adresa = gymData["adresa"] != null ? gymData["adresa"].ToString() : string.Empty;
                    gym.grad = gymData["grad"] != null ? gymData["grad"].ToString() : string.Empty;
                    gym.naziv = gymData["naziv"] != null ? gymData["naziv"].ToString() : string.Empty;
                    gym.telefon = gymData["telefon"] != null ? gymData["telefon"].ToString() : string.Empty;
                    gym.tip = gymData["tip"] != null ? gymData["tip"].ToString() : string.Empty;
                    teretane.Add(gym);
                }

            }

            return teretane;
        }

        public static void AddGym(Teretana t)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1= "insert into \"HealthRate\".\"Teretana\" (\"objekatID\", adresa, grad, naziv, telefon, tip)  values ('" + t.objekatID + "','" + t.adresa + "','" + t.grad + "','" + t.naziv + "','" + t.telefon + "','" + t.tip + "')";
            RowSet gymData = session.Execute(query1);

        }

        public static void DeleteGym(Teretana gym)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet gymData = session.Execute("delete from \"HealthRate\".\"Teretana\" where \"objekatID\" = '" + gym.objekatID + "'");

        }

        public static void PutGym(Teretana gym)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1 = "UPDATE \"HealthRate\".\"Teretana\" SET \"adresa\" = '" + gym.adresa + "'" + "grad\" = '" + gym.grad + "'" + "naziv\" = '" + gym.naziv + "'" + "tip\" = '" + gym.tip + "'" + "telefon\" = '" + gym.telefon + "' WHERE \"objekatID\" = '" + gym.objekatID + "'";
            RowSet bazenData = session.Execute(query1);

        }
        #endregion

        #region Musterija

        public static Musterija GetMusterija(string tel)
        {
            ISession session = SessionManager.GetSession();
            Musterija m = new Musterija();

            if (session == null)
                return null;

            Row mData = session.Execute("select * from \"HealthRate\".\"Musterija\" where telefon='" + tel + "'").FirstOrDefault();

            if (mData != null)
            {
                m.telefon = mData["telefon"] != null ? mData["telefon"].ToString() : string.Empty;
                m.email = mData["email"] != null ? mData["email"].ToString() : string.Empty;
                m.ime = mData["ime"] != null ? mData["ime"].ToString() : string.Empty;
                m.prezime = mData["prezime"] != null ? mData["prezime"].ToString() : string.Empty;
            }

            return m;
        }

        public static List<Musterija> GetMusterije()
        {
            ISession session = SessionManager.GetSession();
            List<Musterija> musterije = new List<Musterija>();

            if (session == null)
                return null;

            RowSet mustData = session.Execute("select * from \"HealthRate\".\"Musterija\"");

            if (mustData.Count() > 0)
            {
                foreach (Row mData in mustData)
                {
                    Musterija m = new Musterija();
                    m.telefon = mData["telefon"] != null ? mData["telefon"].ToString() : string.Empty;
                    m.email = mData["email"] != null ? mData["email"].ToString() : string.Empty;
                    m.ime = mData["ime"] != null ? mData["ime"].ToString() : string.Empty;
                    m.prezime = mData["prezime"] != null ? mData["prezime"].ToString() : string.Empty;

                    musterije.Add(m);
                }
            }

            return musterije;
        }

        public static void AddMusterija(Musterija m)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query = "insert into \"HealthRate\".\"Musterija\" (\"telefon\", email, ime, prezime)  values ('" + m.telefon + "','" + m.email + "','" + m.ime + "','" + m.prezime + "')";
            RowSet mData = session.Execute(query);
        }

        public static void DeleteMusterija(Musterija m)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet mData = session.Execute("delete from \"HealthRate\".\"Musterija\" where phone = '" + m.telefon + "'");
        }

        public static void PutMusterija(Musterija m)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1 = "UPDATE \"HealthRate\".\"Musterija\" SET \"telefon\" = '" + m.telefon + "'" + "ime\" = '" + m.ime + "'" + "prezime\" = '" + m.prezime + "'" + "email\" = '" + m.email + "'";
            RowSet musterijaData = session.Execute(query1);

        }
        #endregion

        #region Rezervacija

        public static Rezervacija GetRezervacija(string rezID)
        {
            ISession session = SessionManager.GetSession();
            Rezervacija rez = new Rezervacija();

            if (session == null)
                return null;

            Row rezData = session.Execute("select * from \"HealthRate\".\"Rezervacija\" where \"rezID\"='" + rezID + "'").FirstOrDefault();

            if (rezData != null)
            {
                rez.radnikID = rezData["radnikID"] != null ? rezData["radnikID"].ToString() : string.Empty;
                rez.objekatID = rezData["objekatID"] != null ? rezData["objekatID"].ToString() : string.Empty;
                rez.rezID = rezData["rezID"] != null ? rezData["rezID"].ToString() : string.Empty;
                rez.vreme_dolaska = rezData["vreme_dolaska"] != null ? rezData["vreme_dolaska"].ToString() : string.Empty;
                rez.vreme_odlaska = rezData["vreme_odlaska"] != null ? rezData["vreme_odlaska"].ToString() : string.Empty;
                rez.naziv_usluge = rezData["naziv_usluge"] != null ? rezData["naziv_usluge"].ToString() : string.Empty;
                rez.ocena_usluge = rezData["ocena_usluge"] != null ? rezData["ocena_usluge"].ToString() : string.Empty;
                rez.musterija = rezData["telefon_musterija"] != null ? rezData["telefon_musterija"].ToString() : string.Empty;

            }

            return rez;
        }

        public static List<Rezervacija> GetRezervacije()
        {
            ISession session = SessionManager.GetSession();
            List<Rezervacija> rezervacije = new List<Rezervacija>();

            if (session == null)
                return null;

            RowSet rezervacijeData = session.Execute("select * from \"HealthRate\".\"Rezervacija\"");

            if (rezervacijeData.Count() > 0)
            {
                foreach (Row rezData in rezervacijeData)
                {
                    Rezervacija rez = new Rezervacija();
                    rez.radnikID = rezData["radnikID"] != null ? rezData["radnikID"].ToString() : string.Empty;
                    rez.objekatID = rezData["objekatID"] != null ? rezData["objekatID"].ToString() : string.Empty;
                    rez.rezID = rezData["rezID"] != null ? rezData["rezID"].ToString() : string.Empty;
                    rez.vreme_dolaska = rezData["vreme_dolaska"] != null ? rezData["vreme_dolaska"].ToString() : string.Empty;
                    rez.vreme_odlaska = rezData["vreme_odlaska"] != null ? rezData["vreme_odlaska"].ToString() : string.Empty;
                    rez.naziv_usluge = rezData["naziv_usluge"] != null ? rezData["naziv_usluge"].ToString() : string.Empty;
                    rez.ocena_usluge = rezData["ocena_usluge"] != null ? rezData["ocena_usluge"].ToString() : string.Empty;
                    rez.musterija = rezData["telefon_musterija"] != null ? rezData["telefon_musterija"].ToString() : string.Empty;
                    rezervacije.Add(rez);
                }
            }

            return rezervacije;
        }

        public static void AddRezervcija(Rezervacija r)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query = "insert into \"HealthRate\".\"Rezervacija\" (\"rezID\", vreme_dolaska, vreme_odlaska, radnikID, naziv_usluge, objekatID, ocena_usluge, telefon_musterija)  values ('" + r.rezID + "','" +r.vreme_dolaska + "','" + r.vreme_odlaska + "','" + r.radnikID + "','" + r.naziv_usluge + "','" + r.objekatID + "','" + r.ocena_usluge + "','" + r.musterija + "')";
            RowSet rezData = session.Execute(query);

        }

        public static void DeleteRezervacija(Rezervacija rez)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet rezData = session.Execute("delete from \"HealthRate\".\"Rezervacija\" where \"rezID\" = '" + rez.rezID + "'");

        }

        public static void PutRezervacija(Rezervacija r)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1 = "UPDATE \"HealthRate\".\"Rezervacija\" SET \"vreme_dolaska\" = '" + r.vreme_dolaska + "'" + "vreme_odlaska\" = '" + r.vreme_odlaska + "'" + "objekatID\" = '" + r.objekatID + "'" + "naziv_usluge\" = '" + r.naziv_usluge + "'" + "ocena_usluge\" = '" + r.ocena_usluge + "radnikID\" = '" + r.radnikID +"telefon_musterija\" = '" + r.musterija+ "' WHERE \"rezID\" = '" + r.rezID + "'";
            RowSet rezData = session.Execute(query1);

        }
        #endregion

        #region Zaposleni
        public static Zaposleni GetZaposlen(string radnikID, string objekatID)
        {
            ISession session = SessionManager.GetSession();
            Zaposleni z = new Zaposleni();

            if (session == null)
                return null;

            Row zData = session.Execute("select * from \"HealthRate\".\"Zaposleni\" where \"radnikID\"='" + radnikID + "' and \"objekatID\"='" + objekatID + "'").FirstOrDefault();

            if (zData != null)
            {
                z.radnikID = zData["radnikID"] != null ? zData["radnikID"].ToString() : string.Empty;
                z.objekatID = zData["objekatID"] != null ? zData["objekatID"].ToString() : string.Empty;
                z.ime = zData["ime"] != null ? zData["ime"].ToString() : string.Empty;
                z.prezime = zData["prezime"] != null ? zData["prezime"].ToString() : string.Empty;
                z.pol = zData["pol"] != null ? zData["pol"].ToString() : string.Empty;
                z.godine_iskustva = zData["godine_iskustva"] != null ? zData["godine_iskustva"].ToString() : string.Empty;
                string ocena = zData["prosecna_ocena"] != null ? zData["prosecna_ocena"].ToString() : string.Empty;
                if (ocena != "")
                    z.prosecna_ocena = Double.Parse(ocena); 
                else z.prosecna_ocena = 0.0;
            }

            return z;
        }

        public static List<Zaposleni> GetZaposleni()
        {
            ISession session = SessionManager.GetSession();
            List<Zaposleni> zaposleni = new List<Zaposleni>();

            if (session == null)
                return null;

            RowSet zaposleniData = session.Execute("select * from \"HealthRate\".\"Zaposleni\"");

            if (zaposleniData.Count() > 0)
            {
                foreach (Row zData in zaposleniData)
                {
                    Zaposleni z = new Zaposleni();
                    z.radnikID = zData["radnikID"] != null ? zData["radnikID"].ToString() : string.Empty;
                    z.objekatID = zData["objekatID"] != null ? zData["objekatID"].ToString() : string.Empty;
                    z.ime = zData["ime"] != null ? zData["ime"].ToString() : string.Empty;
                    z.prezime = zData["prezime"] != null ? zData["prezime"].ToString() : string.Empty;
                    z.pol = zData["pol"] != null ? zData["pol"].ToString() : string.Empty;
                    z.godine_iskustva = zData["godine_iskustva"] != null ? zData["godine_iskustva"].ToString() : string.Empty;
                    string ocena = zData["prosecna_ocena"] != null ? zData["prosecna_ocena"].ToString() : string.Empty;
                    if (ocena != "")
                        z.prosecna_ocena = Double.Parse(ocena);
                    else z.prosecna_ocena = 0.0;

                    zaposleni.Add(z);
                }
            }

            return zaposleni;
        }

        public static void AddZaposlen(Zaposleni radnik)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
            string query = "insert into \"HealthRate\".\"Zaposleni\" (\"radnikID\", \"objekatID\", ime, prezime, pol, godine_iskustva, \"struka\", prosecna_ocena)  values ('" + radnik.radnikID + "','" + radnik.objekatID + "','" + radnik.ime + "','" + radnik.prezime + "','" + radnik.pol + "','" + radnik.godine_iskustva + "','" + radnik.struka + "','" + radnik.prosecna_ocena.ToString() + "')";
            RowSet zData = session.Execute(query);

        }

        public static void DeleteZaposlen(Zaposleni radnik)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet zData = session.Execute("delete from \"HealthRate\".\"Zaposleni\" where \"radnikID\" = '" + radnik.radnikID + "' and \"objekatID\" = '" + radnik.objekatID + "'");

        }

        public static void DeleteZaposlen(string radnikID, string objekatID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet zData = session.Execute("delete from \"HealthRate\".\"Zaposleni\" where \"radnikID\" = '" + radnikID + "' and \"objekatID\" = '" + objekatID + "'");

        }

        public static void PutZaposlen(Zaposleni z)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            string query1 = "UPDATE \"HealthRate\".\"Zaposleni\" SET \"ime\" = '" + z.ime + "'" + "prezime\" = '" + z.prezime + "'" + "prosecna_ocena\" = '" + z.prosecna_ocena + "'" + "struka\" = '" + z.struka + "'" + "pol\" = '" + z.pol + "'" + "godine_iskustva\" = '" + z.godine_iskustva + "' WHERE \"radnikID\" = '" + z.radnikID +"' AND \"objekatID\" = '" + z.objekatID + "'";
            RowSet bazenData = session.Execute(query1);

        }
        #endregion

    }
}
